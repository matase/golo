class User < ApplicationRecord
  has_secure_password
  has_one :master_goal
  validates :username, presence: true, uniqueness: true
  validates :password, presence: true
end
