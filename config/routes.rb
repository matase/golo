Rails.application.routes.draw do
  resources :users, only: [:create, :show]
  get "/welcome", to: "layouts#index"
  get "/signup", to: "users#new"
  get "/login", to: "sessions#new"
  post "/sessions", to: "sessions#create"
  delete "/sessions", to: "sessions#destroy"
  root 'layouts#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users do
    resources :master_goals
  end
end
